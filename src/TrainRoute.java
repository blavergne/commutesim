import java.util.*;

class TrainRoute {

	private static List<Station> redlineRoute = new LinkedList<Station>();

	TrainRoute() {
		redlineRoute.add(new Station("Howard", 30, null));
		redlineRoute.add(new Station("Jarvis", 30, null));
		redlineRoute.add(new Station("Morse", 30, null));

		for (int i = 0; i < redlineRoute.size() - 1; i++) {
			redlineRoute.get(i).setNextStation(redlineRoute.get(i + 1));
		
			if(i != 0) {
				redlineRoute.get(i).setPrevStation(redlineRoute.get(i - 1));
			}
		}
	}

	// getters
	public List<Station> getRedlineRoute() {
		return redlineRoute;
	}

	//setters
	public void setRedlineRoute(List<Station> var) {
		redlineRoute = var;
	}
	
	// methods
	public void addPassengerToStation(Passenger passenger, String stationName) {
		for (Station station : redlineRoute) {
			if (station.getStationName().equals(stationName)) {
				station.addPassenger(passenger);
			}
		}
	}

	public void reverseTrainRoute() {

		List<Station> reversedRoute = new LinkedList<Station>();
		int routeSize = redlineRoute.size();

		for (int i = 0; i < routeSize; i++) 
			reversedRoute.add(redlineRoute.get((routeSize - 1) - i));	
		
		for(int i = 0; i < routeSize - 1; i++) {
			reversedRoute.get(i).setNextStation(reversedRoute.get(i + 1));
			
			if(i != 0) 
				reversedRoute.get(i).setPrevStation(reversedRoute.get(i - 1));
	
		}
	
		reversedRoute.get(routeSize - 1).nextStation = null;
		redlineRoute = reversedRoute;
		
	}

}