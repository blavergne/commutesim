import java.util.*;

class Train extends Thread {

	// Trains will have one of two directions; North or South, a color and list of
	// stations
	String color;
	String direction;
	List<Passenger> currentPassengers = new LinkedList<Passenger>();
	Station currentStation;
	Station startingStation;
	TrainRoute trainRoute;
	int trainSpeed;
	int distanceToNextStation;
	boolean doorsOpen;

	Train(String color, String direction, Station startingStation, TrainRoute trainRoute) {
		trainSpeed = 10; // feet per second
		doorsOpen = false;
		this.trainRoute = trainRoute;
		this.color = color;
		this.direction = direction;
		this.startingStation = startingStation;
	}

	public String getColor() {
		return color;
	}

	public String getDirection() {
		return direction;
	}

	public Station getCurrentStation() {
		return currentStation;
	}

	public Station getStartingStation() {
		return startingStation;
	}

	public int getDistanceToNextStation() {
		return currentStation.getDistanceToNextStation();
	}

	public List<Passenger> getCurrentPassengers() {
		return currentPassengers;
	}

	// setters
	public void setDirection(String var) {
		direction = var;
	}

	public void setCurrentStation(Station var) {
		currentStation = var;
	}

	// methods
	public void addPassenger(Passenger var) {
		currentPassengers.add(var);
	}

	public void removePassenger(Passenger var) {
		currentPassengers.remove(var);
	}

	public void waitingAtStation(Station station) {
		Date startTime = new Date();
		Date endTime = startTime;
		int boardTime = 6;

		List<Passenger> passengerList = station.getCurrentlyWaitingPassengers();

		while ((int) ((endTime.getTime() - startTime.getTime()) / 1000) < boardTime) {
			System.out.println("Waiting at " + station.getStationName());

			// Passengers in train exit train if their stop
			if (!currentPassengers.isEmpty()) {
				for (Passenger passenger : currentPassengers) {
					if (passenger.getDestination().equals(station.getStationName())) {
						System.out.println(passenger.getPassengerName() + " reached their destination and exited the "
								+ color + " train.");
						currentPassengers.remove(passenger);
					}
				}
			}

			// Passengers at station board train
			if (!passengerList.isEmpty()) {
				for (Passenger passenger : passengerList) {
					System.out.println(passenger.getPassengerName() + " boarded the " + color + " train.");
					addPassenger(passenger);
					passengerList.remove(passenger);
				}
			}
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				System.out.println(e);
			}
			endTime = new Date();
		}

	}

	public void traveling(TrainRoute trainRoute) {

		while (true) {
		
			for (Station station : trainRoute.getRedlineRoute()) {

				System.out.println("All aboard");
				station.setTrainInStation(true);
				doorsOpen = true;
				waitingAtStation(station);
				doorsOpen = false;
				station.setTrainInStation(false);
				System.out.println("Doors are closing");
				
				if (station.getNextStation() == null) {
					trainRoute.reverseTrainRoute();
					break;
				}

				Date startTime = new Date();
				Date endTime = startTime;
				int timeToDestination = station.getDistanceToNextStation() / trainSpeed;
				System.out.println("Leaving " + station.getStationName() + "." + " Next stop is "
						+ station.getNextStation().getStationName());

				while ((int) ((endTime.getTime() - startTime.getTime()) / 1000) < timeToDestination) {
					System.out.println("Traveling to " + station.getNextStation().getStationName());
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						System.out.println(e);
					}
					endTime = new Date();
				}

			}

		}
	}

}