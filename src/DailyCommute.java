
class DailyCommute {

	public static void main(String[] args) {

		TrainRoute trainRoute = new TrainRoute();
		trainRoute.addPassengerToStation(new Passenger("Brian", "Morse"), "Howard");
		trainRoute.addPassengerToStation(new Passenger("Claudia", "Jarvis"), "Howard");
		trainRoute.addPassengerToStation(new Passenger("Ben", "Morse"), "Jarvis");
		Train redline = new Train("Red", "inbound", null, null);
		redline.traveling(trainRoute);

	}

}