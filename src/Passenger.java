class Passenger {

	String name;
	String destination;
	String trainCurrentlyRiding;
	String currentStation;

	Passenger(String name, String destination) {
		this.name = name;
		this.destination = destination;
	}

	public String getPassengerName() {
		return name;
	}

	public String getDestination() {
		return destination;
	}

	public String getCurrentTrainName() {
		return trainCurrentlyRiding;
	}

	public String getCurrentStation() {
		return currentStation;
	}

	// setters
	public void setDestination(String var) {
		destination = var;
	}

	public void setCurrentTrainName(String var) {
		trainCurrentlyRiding = var;
	}

}