import java.util.LinkedList;
import java.util.List;

class Station {

	String stationName;
	String[] trainConnections;
	List<Passenger> currentWaitingPassengers = new LinkedList<Passenger>();// TODO if train in station then iterate
																			// through waiting passenger to see who
																			// wants to get on.
	int pickupTime;
	int distanceToNextStation;
	Station nextStation;
	Station prevStation;
	boolean trainInStation;

	Station(String stationName, int pickupTime, String[] trainConnections) {
		this.stationName = stationName;
		this.pickupTime = pickupTime;
		this.trainConnections = trainConnections;
		trainInStation = false;
		distanceToNextStation = 200;
	}

	// getters
	public Station getNextStation() {
		return nextStation;
	}

	public Station getPrevStation() {
		return prevStation;
	}

	public String getStationName() {
		return stationName;
	}

	public int getPickupTime() {
		return pickupTime;
	}

	public List<Passenger> getCurrentlyWaitingPassengers() {
		return currentWaitingPassengers;
	}

	public String[] getTrainConnections() {
		return trainConnections;
	}

	public int getDistanceToNextStation() {
		return distanceToNextStation;
	}

	public boolean getTrainInStation() {
		return trainInStation;
	}

	// setters
	public void setNextStation(Station var) {
		nextStation = var;
	}

	public void setPrevStation(Station var) {
		prevStation = var;
	}

	public void setPickupTime(int var) {
		pickupTime = var;
	}

	public void setDistanceToNextStation(int var) {
		distanceToNextStation = var;
	}

	public void setTrainInStation(boolean var) {
		trainInStation = var;
	}

	// methods
	public void addPassenger(Passenger var) {
		currentWaitingPassengers.add(var);
	}

	public void removePassenger(Passenger var) {
		currentWaitingPassengers.remove(var);
	}

}